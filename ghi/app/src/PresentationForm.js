import { useState, useEffect } from "react";

function PresentationForm() {
    const [conferences, setConferences] = useState([])
    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [companyName, setCompanyName] = useState()
    const [title, setTitle] = useState()
    const [synopsis, SetSynopsis] = useState()
    const [conference, setConference] = useState()

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.presenter_email = email;
        data.presenter_name = name
        data.synopsis = synopsis
        data.title = title
        data.company_name = companyName
        console.log("conference::  ",conference)
        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`
        console.log("URL::  ",presentationUrl)
        const fetchOptions = {
          method: 'post',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const presentationResponse = await fetch(presentationUrl, fetchOptions);
        if (presentationResponse.ok) {
          setConference('');
          setName('');
          setEmail('');
          setTitle('')
          SetSynopsis('')
          setCompanyName('')

        }
      }

    const handleConferenceChange = (event) => {
        const value = event.target.value
        setConference(value)
    }
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handleEmailChange = (event) => {
        const value = event.target.value
        setEmail(value)
    }
    const handleCompanyNameChange = (event) => {
        const value = event.target.value
        setCompanyName(value)
    }
    const handleTitleChange = (event) => {
        const value = event.target.value
        setTitle(value)
    }
    const handleSynopsisChange = (event) => {
        const value = event.target.value
        SetSynopsis(value)
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setConferences(data.conferences);
        }
      }

      useEffect(() => {
        fetchData();
      }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Presenter name" required type="text" id="presenter_name" name="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmailChange} placeholder="Presenter email" type="email" id="presenter_email" name="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} placeholder="Company name" type="text" id="company_name" name="company_name" className="form-control" />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input  onChange={handleTitleChange}placeholder="Title" required type="text" id="title" name="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label onChange={handleSynopsisChange} htmlFor="synopsis" className="form-label">Synopsis</label>
                <textarea className="form-control" required id="synopsis" name="synopsis" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} required id="conference" name="conference" className="form-select">
                  <option value={conference}>Choose a conference</option>
                  {
                    conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.id}>
                                {conference.name}
                            </option>
                        )
                    })
                  }
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}


export default PresentationForm;
