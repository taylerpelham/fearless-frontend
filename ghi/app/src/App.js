import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import { Routes, Route, BrowserRouter } from 'react-router-dom';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App() {

  return (
    <>
    <BrowserRouter>
    <Nav />
    <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm />} />
            <Route path="" element={<AttendeesList />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
